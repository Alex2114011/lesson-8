//
//  CustomHeaderCell.swift
//  ExpandebleSectionsHW7
//
//  Created by Alex on 16.03.2022.
//

import UIKit

final class CustomHeaderCell: UITableViewHeaderFooterView {

    //MARK: - Callbacks

    var didTapExpandButton:(() -> ())?

    //MARK: - Private property

    private lazy var genreName: UILabel = {
        let label = UILabel()
        label.text = "rock"
        label.font = .systemFont(ofSize: 22, weight: .bold)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private lazy var expandButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "arrow"), for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.tintColor = .white
        button.layer.cornerRadius = 4
        button.addTarget(self, action: #selector(didTapArrow), for: .touchUpInside)
        return button
    }()

    //MARK: - init

    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        setupUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    //MARK: - Public methods

    func configureHeaderCell(with headerText: String, section: Int) {
        genreName.text = headerText
        expandButton.tag = section
    }

    func rotateImage(_ expanded: Bool) {
        if expanded {
            expandButton.imageView?.transform = CGAffineTransform(rotationAngle: CGFloat.zero)
        } else {
            expandButton.imageView?.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
        }
    }

}

//MARK: - Private methods

extension CustomHeaderCell {

    private func setupUI() {
        contentView.addSubview(genreName)
        contentView.addSubview(expandButton)
        NSLayoutConstraint.activate([genreName.topAnchor.constraint(equalTo: contentView.topAnchor),
                                     genreName.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),

                                     expandButton.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
                                     expandButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
                                     expandButton.widthAnchor.constraint(equalToConstant: 25),
                                     expandButton.heightAnchor.constraint(equalToConstant: 25)
                                    ])
        
    }




    @objc private func didTapArrow() {
        didTapExpandButton?()
    }
}
