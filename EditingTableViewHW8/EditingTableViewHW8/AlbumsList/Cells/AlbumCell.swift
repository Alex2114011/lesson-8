//
//  AlbumCell.swift
//  ExpandebleSectionsHW7
//
//  Created by Alex on 16.03.2022.
//

import UIKit

final class AlbumCell: UITableViewCell {

    //MARK: - Private property

    private lazy var coverAlbum: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()

    private lazy var nameAlbum: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private lazy var ratingCount: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private lazy var ratingAlbum: UILabel = {
        let label = UILabel()
        label.text = "Rating:"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private lazy var addButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage.init(systemName: "plus"), for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.tintColor = .white
        button.layer.cornerRadius = 4
        button.backgroundColor = UIColor.init(red: 214/255, green: 0, blue: 23/255, alpha: 1)
        return button
    }()

    //MARK: - init

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    //MARK: - Public methods

    func configureCell(with model: Album) {
        if let imageName = model.nameCover {
            self.coverAlbum.image = UIImage(named: imageName)
        } else {
            if let imageData = model.imageCover {
                self.coverAlbum.image = UIImage(data: imageData)
            }
        }
        guard let nameAlbum = model.nameAlbum,
              let ratingCount = model.ratingAlbumCount else { return}
        self.nameAlbum.text = nameAlbum
        self.ratingCount.text = ratingCount
    }
}

//MARK: - Private methods

extension AlbumCell {

    private func setupUI() {
        [coverAlbum, nameAlbum, ratingAlbum, ratingCount, addButton].forEach { contentView.addSubview($0) }
        self.selectionStyle = .none
        NSLayoutConstraint.activate([coverAlbum.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 20),
                                     coverAlbum.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
                                     coverAlbum.heightAnchor.constraint(equalToConstant: 75),
                                     coverAlbum.widthAnchor.constraint(equalToConstant: 75),
                                     coverAlbum.bottomAnchor.constraint(lessThanOrEqualTo: contentView.bottomAnchor, constant: -20),

                                     nameAlbum.topAnchor.constraint(equalTo: coverAlbum.topAnchor),
                                     nameAlbum.leadingAnchor.constraint(equalTo: coverAlbum.trailingAnchor, constant: 20),
                                     nameAlbum.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),

                                     addButton.topAnchor.constraint(equalTo: nameAlbum.bottomAnchor, constant: 10),
                                     addButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
                                     addButton.heightAnchor.constraint(greaterThanOrEqualToConstant: 24),
                                     addButton.widthAnchor.constraint(equalToConstant: 70),
                                     addButton.bottomAnchor.constraint(lessThanOrEqualTo: contentView.bottomAnchor, constant: -20),

                                     ratingAlbum.topAnchor.constraint(equalTo: nameAlbum.bottomAnchor,constant: 10),
                                     ratingAlbum.leadingAnchor.constraint(equalTo: coverAlbum.trailingAnchor, constant: 20),
                                     ratingAlbum.trailingAnchor.constraint(equalTo: ratingCount.leadingAnchor, constant: -5),
                                     ratingAlbum.bottomAnchor.constraint(lessThanOrEqualTo: contentView.bottomAnchor, constant: -20),

                                     ratingCount.topAnchor.constraint(equalTo: nameAlbum.bottomAnchor,constant: 10)
                                    ])
    }
}
