//
//  ViewController.swift
//  ExpandebleSectionsHW7
//
//  Created by Alex on 16.03.2022.
//

import UIKit

final class AlbumsTableViewController: UIViewController {
    
    //MARK: - Private property

    private var dataSource: [Genre] = [
        Genre(nameGenre: "Rock",
              album: [
                Album(imageCover: nil, nameCover: "Rock1",
                      nameAlbum: "Sleep Through the Static",
                      ratingAlbumCount: "4.2"),
                Album(imageCover: nil, nameCover: "Rock2",
                      nameAlbum: "For performance reasons, a table view’s delegate should generally reuse UITableViewHeaderFooterView",
                      ratingAlbumCount: "2.3"),
                Album(imageCover: nil, nameCover: "Rock3",
                      nameAlbum: "To the Sea",
                      ratingAlbumCount: "4.2"),
                Album(imageCover: nil, nameCover: "Rock4",
                      nameAlbum: "In Between Dreams",
                      ratingAlbumCount: "2.3")],
              isExpanded: false),
        Genre(nameGenre: "Pop",
              album: [
                Album(imageCover: nil, nameCover: "Pop1",
                      nameAlbum: "So in Love",
                      ratingAlbumCount: "4.2"),
                Album(imageCover: nil, nameCover: "Pop2",
                      nameAlbum: "Wasting Time - Single",
                      ratingAlbumCount: "2.3"),
                Album(imageCover: nil, nameCover: "Pop3",
                      nameAlbum: "On and On",
                      ratingAlbumCount: "4.2"),
                Album(imageCover: nil, nameCover: "Pop4",
                      nameAlbum: "Keep Dancing",
                      ratingAlbumCount: "4.2")],
              isExpanded: false),
        Genre(nameGenre: "Holiday",
              album: [
                Album(imageCover: nil, nameCover: "Holiday1",
                      nameAlbum: "In the Morning - Single",
                      ratingAlbumCount: "4.2"),
                Album(imageCover: nil, nameCover: "Holiday2",
                      nameAlbum: "Rudolph the Red Nosed Reindeer (Live from Late Night with Jimmy Fallon) - Single",
                      ratingAlbumCount: "2.3"),
                Album(imageCover: nil, nameCover: "Holiday3",
                      nameAlbum: "The Captain Is Drunk - Single",
                      ratingAlbumCount: "4.2"),
                Album(imageCover: nil, nameCover: "Holiday4",
                      nameAlbum: "Doctor Sleep & The Shining 2-Film Collection",
                      ratingAlbumCount: "4.2")],
              isExpanded: false),
        Genre(nameGenre: "Action & Adventure",
              album: [
                Album(imageCover: nil, nameCover: "Action&Adventure1",
                      nameAlbum: "Jumanji Bienvenue Dans La Jungle + Jumanji Next Level",
                      ratingAlbumCount: "4.2"),
                Album(imageCover: nil, nameCover: "Action&Adventure2",
                      nameAlbum: "Star Wars: The Skywalker Saga 9-Movie Collection",
                      ratingAlbumCount: "2.3"),
                Album(imageCover: nil, nameCover: "Action&Adventure3",
                      nameAlbum: "Smart Comedies",
                      ratingAlbumCount: "4.2"),
                Album(imageCover: nil, nameCover: "Action&Adventure4",
                      nameAlbum: "The Outpost",
                      ratingAlbumCount: "4.2")],
              isExpanded: false)]
    
    private lazy var albumsTableView: UITableView = {
        let table = UITableView(frame: .zero, style: .grouped)
        table.dataSource = self
        table.delegate = self
        table.register(AlbumCell.self,
                       forCellReuseIdentifier: String(describing: AlbumCell.self))
        table.register(CustomHeaderCell.self,
                       forHeaderFooterViewReuseIdentifier:  String(describing: CustomHeaderCell.self))
        table.translatesAutoresizingMaskIntoConstraints = false
        return table
    }()

    //MARK: - Life cicle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
}

//MARK: - Private methods

extension AlbumsTableViewController {
    private func setupUI() {
        view.backgroundColor = .systemGroupedBackground
        view.addSubview(albumsTableView)

        navigationItem.title = "Albums"
        navigationController?.navigationBar.prefersLargeTitles = true

        let navItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(openAddAlbumViewController))
        navigationItem.rightBarButtonItem = navItem
        NSLayoutConstraint.activate([albumsTableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
                                     albumsTableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                                     albumsTableView.trailingAnchor .constraint(equalTo: view.trailingAnchor),
                                     albumsTableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)])
    }

    @objc private func openAddAlbumViewController() {
        let vc = AddingAlbumViewController()
        vc.delegate = self
        navigationController?.pushViewController(vc, animated: true)
    }

    private func turnEditing() {
        albumsTableView.isEditing.toggle()
    }
}

//MARK: - UITableViewDataSource

extension AlbumsTableViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataSource.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dataSource[section].isExpanded {
            return dataSource[section].album.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = dataSource[indexPath.section].album[indexPath.row]
        let cell = albumsTableView.dequeueReusableCell(withIdentifier: String(describing: AlbumCell.self), for: indexPath) as! AlbumCell
        cell.configureCell(with: item)
        return cell
    }
}

//MARK: - UITableViewDelegate

extension AlbumsTableViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let item = dataSource[section]
        let header = albumsTableView.dequeueReusableHeaderFooterView(
            withIdentifier: String(describing: CustomHeaderCell.self)) as! CustomHeaderCell
        
        header.configureHeaderCell(with: item.nameGenre, section: section)
        
        header.didTapExpandButton = { [weak self] in
            guard let self = self else { return }
            
            var indexPaths = [IndexPath]()
            
            for row in self.dataSource[section].album.indices {
                let indexPath = IndexPath(row: row, section: section)
                indexPaths.append(indexPath)
            }
            
            let isExpanded = self.dataSource[section].isExpanded
            header.rotateImage(isExpanded)
            self.dataSource[section].isExpanded = !isExpanded
            
            if isExpanded {
                self.albumsTableView.deleteRows(at: indexPaths, with: .left)
            } else {
                self.albumsTableView.insertRows(at: indexPaths, with: .left)
            }
        }
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }

    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { _,_,_ in
            self.dataSource[indexPath.section].album.remove(at: indexPath.row)
            self.albumsTableView.deleteRows(at: [indexPath], with: .left)
        }

        let moveAction = UIContextualAction(style: .normal, title: "Move") { (_, _, handler) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.9) {
                self.turnEditing()
            }
            handler(true)
        }
        moveAction.backgroundColor = .orange

        let swipeActions = UISwipeActionsConfiguration(actions: [deleteAction, moveAction])
        return swipeActions
    }

    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        self.dataSource[destinationIndexPath.section].album.insert(dataSource[sourceIndexPath.section].album[sourceIndexPath.row], at: destinationIndexPath.row)
        self.dataSource[sourceIndexPath.section].album.remove(at: sourceIndexPath.row)
        albumsTableView.reloadData()
    }
}

//MARK: - AddDelegate

extension AlbumsTableViewController: AddAlbumDelegate {
    func addNewAlbum(from datasource: Genre) {
        var index = -1
        for item in dataSource {
            index += 1
            if item.nameGenre == datasource.nameGenre {
                dataSource[index].album.append(contentsOf: datasource.album)
            }
        }
        albumsTableView.reloadData()
    }
}
