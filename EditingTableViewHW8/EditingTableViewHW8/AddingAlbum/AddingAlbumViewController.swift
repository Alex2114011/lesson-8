//
//  AddingAlbumViewController.swift
//  EditingTableViewHW8
//
//  Created by Alex on 19.03.2022.
//

import UIKit

protocol AddAlbumDelegate: AnyObject {
    func addNewAlbum(from datasource: Genre)
}

final class AddingAlbumViewController: UIViewController {

    //MARK: - Delegate

    weak var delegate: AddAlbumDelegate?

    //MARK: - Private property

    private var imageData: Data?
    private var nameAlbum: String?
    private var rating: String?
    private var genre: String?

    private lazy var coverImage: UIImage = {
        let image = UIImage(named: "add-photopng")!
        return image
    }()

    private lazy var addingAlbumTableView: UITableView = {
        let table = UITableView(frame: .zero, style: .grouped)
        table.dataSource = self
        table.register(AddingCoverAlbumCell.self, forCellReuseIdentifier: String(describing: AddingCoverAlbumCell.self))
        table.register(SelectGenreAlbumCell.self, forCellReuseIdentifier: String(describing: SelectGenreAlbumCell.self))
        table.register(AddingAttributsAlbumCell.self, forCellReuseIdentifier: String(describing:AddingAttributsAlbumCell.self))
        table.translatesAutoresizingMaskIntoConstraints = false
        table.rowHeight = UITableView.automaticDimension
        return table
    }()

    //MARK: - Life cicle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
}

//MARK: - Private methods

extension AddingAlbumViewController {
    private func setupUI() {
        let navItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(didTapSaveButton))
        navigationItem.rightBarButtonItem = navItem
        view.backgroundColor = .systemGroupedBackground
        addingAlbumTableView.separatorStyle = .none
        view.addSubview(addingAlbumTableView)

        navigationItem.title = "Add new album"
        navigationItem.largeTitleDisplayMode = .never
        NSLayoutConstraint.activate([addingAlbumTableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
                                     addingAlbumTableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                                     addingAlbumTableView.trailingAnchor .constraint(equalTo: view.trailingAnchor),
                                     addingAlbumTableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)])
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(backgroundTap))
        self.view.addGestureRecognizer(tapGestureRecognizer)
    }

    @objc private func backgroundTap(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }

    @objc private func didTapSaveButton() {
        self.view.endEditing(true)
        guard let genre = genre,
              let imageData = imageData,
              let nameAlbum = nameAlbum,
              let rating = rating else {
                  return
              }

        let newAlbum = Genre(nameGenre: genre, album: [Album(imageCover: imageData, nameCover: nil, nameAlbum: nameAlbum, ratingAlbumCount: rating)], isExpanded: false)
        delegate?.addNewAlbum(from: newAlbum)
        navigationController?.popViewController(animated: true)
    }

    private func addImageFromGallery() {
        let imageVC = UIImagePickerController()
        imageVC.sourceType = .photoLibrary
        imageVC.delegate = self
        imageVC.allowsEditing = true
        present(imageVC, animated: true)
    }
}

//MARK: - UITableViewDataSource

extension AddingAlbumViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {

        case 0:
            let cell = tableView.dequeueReusableCell(
                withIdentifier: String(describing: AddingCoverAlbumCell.self),
                for: indexPath) as! AddingCoverAlbumCell
            cell.openGallery = {
                self.addImageFromGallery()
            }
            cell.configureCell(with: coverImage)
            return cell

        case 1:
            let cell = tableView.dequeueReusableCell(
                withIdentifier: String(describing: SelectGenreAlbumCell.self),
                for: indexPath) as! SelectGenreAlbumCell
            cell.saveGenre = { genre in
                self.genre = genre
            }
            return cell

        case 2:
            let cell = tableView.dequeueReusableCell(
                withIdentifier: String(describing: AddingAttributsAlbumCell.self),
                for: indexPath) as! AddingAttributsAlbumCell
            cell.configureCell(with: "Enter name album")
            cell.saveAttributText = { text in
                self.nameAlbum = text
            }
            return cell

        case 3:
            let cell = tableView.dequeueReusableCell(
                withIdentifier: String(describing: AddingAttributsAlbumCell.self),
                for: indexPath) as! AddingAttributsAlbumCell
            cell.configureCell(with: "Enter rating")
            cell.saveAttributText = { text in
                self.rating = text
            }
            return cell
        default:
            break
        }
        return UITableViewCell()
    }
}

// MARK: - UIImagePickerControllerDelegate & UINavigationControllerDelegate

extension AddingAlbumViewController: UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        if let image = info[UIImagePickerController.InfoKey(
            rawValue: "UIImagePickerControllerEditedImage")] as? UIImage {
            coverImage = image
            self.imageData = image.pngData()
            addingAlbumTableView.reloadData()
        }
        picker.dismiss(animated: true, completion: nil)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}

