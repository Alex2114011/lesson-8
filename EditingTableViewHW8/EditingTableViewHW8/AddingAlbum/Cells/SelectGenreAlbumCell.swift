//
//  SelectGenreAlbumCell.swift
//  EditingTableViewHW8
//
//  Created by Alex on 19.03.2022.
//

import UIKit

final class SelectGenreAlbumCell: UITableViewCell {

    //MARK: - Callback

    var saveGenre:((String)->())?

    //MARK: - Private propery

    private var selectedGenre: String?
    private var genreList = ["", "Pop", "Rock", "Holiday", "Action & Adventure"]

    private lazy var selectGenreTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.borderStyle = .roundedRect
        textField.placeholder = "Enter genre"
        return textField
    }()

    //MARK: - Life cicle

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
        createPickerView()
        dismissPickerView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: - Private methods

extension SelectGenreAlbumCell {
    private func setupUI() {
        selectionStyle = .none
        contentView.backgroundColor = .systemGroupedBackground
        contentView.addSubview(selectGenreTextField)
        NSLayoutConstraint.activate([selectGenreTextField.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 20),
                                     selectGenreTextField.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width - 40),
                                     selectGenreTextField.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
                                     selectGenreTextField.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
                                    ])
    }


    private func createPickerView() {
        let pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.selectRow(0, inComponent: 0, animated: true)
        selectGenreTextField.inputView = pickerView
    }

    private func dismissPickerView() {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let button = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneAction))
        toolBar.setItems([button], animated: true)
        toolBar.isUserInteractionEnabled = true
        selectGenreTextField.inputAccessoryView = toolBar
    }

    @objc private func doneAction() {
        self.endEditing(true)
    }

}

//MARK: - UIPickerViewDelegate

extension SelectGenreAlbumCell: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedGenre = genreList[row]
        selectGenreTextField.text = selectedGenre
        if selectGenreTextField.text != "" {
            saveGenre?(selectGenreTextField.text!)
        }
    }


    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return genreList[row]
    }
}

//MARK: - UIPickerViewDataSource

extension SelectGenreAlbumCell: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return genreList.count
    }
}
