//
//  AddingAttributsAlbumCell.swift
//  EditingTableViewHW8
//
//  Created by Alex on 20.03.2022.
//

import UIKit

final class AddingAttributsAlbumCell: UITableViewCell {

    //MARK: - Callbacks

    var saveAttributText:((String)->())?

    //MARK: - Private property

   private lazy var attributeTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.borderStyle = .roundedRect
        textField.delegate = self
        return textField
    }()

    //MARK: - init

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    //MARK: - Public methods

    func configureCell(with textPlaceholder: String) {
        self.attributeTextField.placeholder = textPlaceholder
    }
}

//MARK: - Private methods

extension AddingAttributsAlbumCell {
   private func setupUI(){
        selectionStyle = .none
        contentView.backgroundColor = .systemGroupedBackground
        contentView.addSubview(attributeTextField)
        NSLayoutConstraint.activate([attributeTextField.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 20),
                                     attributeTextField.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
                                     attributeTextField.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
                                     attributeTextField.heightAnchor.constraint(equalToConstant: 34),
                                     attributeTextField.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
                                    ])
    }
}

//MARK: - UITextFieldDelegate

extension AddingAttributsAlbumCell: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        saveAttributText?(textField.text!)
    }
}
