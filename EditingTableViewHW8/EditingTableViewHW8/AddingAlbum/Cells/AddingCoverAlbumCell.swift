//
//  AddingCoverAlbumCell.swift
//  EditingTableViewHW8
//
//  Created by Alex on 19.03.2022.
//

import UIKit

final class AddingCoverAlbumCell: UITableViewCell {

    //MARK: - Callbacks

    var openGallery: (()->())?
    var saveCover:((Data)->())?

    //MARK: - Private property

    private lazy var coverAlbumImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "add-photopng")!
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.isUserInteractionEnabled = true
        imageView.contentMode = .scaleToFill
        let tap = UITapGestureRecognizer(target: self, action: #selector(openGalary))
        imageView.addGestureRecognizer(tap)
        imageView.layer.cornerRadius = 8
        imageView.clipsToBounds = true
        return imageView
    }()

    //MARK: - init

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    //MARK: - Public methods

    func configureCell(with image: UIImage?) {
        coverAlbumImageView.image = image
    }
}

//MARK: - Private methods

extension AddingCoverAlbumCell {

    private func setup() {
        selectionStyle = .none
        contentView.backgroundColor = .systemGroupedBackground
        contentView.addSubview(coverAlbumImageView)

        NSLayoutConstraint.activate([
            coverAlbumImageView.topAnchor.constraint(equalTo: contentView.topAnchor),
            coverAlbumImageView.centerXAnchor.constraint(
                equalTo: contentView.centerXAnchor),
            coverAlbumImageView.centerYAnchor.constraint(
                equalTo: contentView.centerYAnchor),
            coverAlbumImageView.heightAnchor.constraint(
                lessThanOrEqualToConstant: UIScreen.main.bounds.height / 4 ),
            coverAlbumImageView.widthAnchor.constraint(
                lessThanOrEqualToConstant: UIScreen.main.bounds.width - 20 - 20)
        ])
    }

    @objc private func openGalary() {
        openGallery?()
    }
}

