//
//  CellModel.swift
//  ExpandebleSectionsHW7
//
//  Created by Alex on 16.03.2022.
//

import Foundation

struct Genre {
    let nameGenre: String
    var album: [Album]
    var isExpanded: Bool
}

struct Album {
    let imageCover: Data?
    let nameCover: String?
    let nameAlbum: String?
    let ratingAlbumCount: String?
}
