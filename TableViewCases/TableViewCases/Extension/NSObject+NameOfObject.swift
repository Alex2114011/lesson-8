//
//  NSObject+NameOfObject.swift
//  TableViewCases
//
//  Created by Курганов Сергей Владимирович on 21.09.2021.
//

import Foundation

extension NSObject {
    @objc class var nameOfClass: String {
        NSStringFromClass(self).components(separatedBy: ".").last ?? ""
    }
    
    var nameOfClass: String {
        NSStringFromClass(type(of: self)).components(separatedBy: ".").last ?? ""
    }
}
