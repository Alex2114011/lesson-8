//
//  CollectionView+Register.swift
//  TableViewCases
//
//  Created by Курганов Сергей Владимирович on 21.09.2021.
//

import UIKit

extension UICollectionView {
    func registerCell(from cellType: Swift.AnyClass) {
        register(cellType.self, forCellWithReuseIdentifier: cellType.nameOfClass)
    }
    
    func dequeueReusableCell<Cell: UICollectionViewCell>(_ cellType: Cell.Type,
                                                         indexPath: IndexPath) -> Cell {
        guard let cell = dequeueReusableCell(withReuseIdentifier: cellType.nameOfClass,
                                             for: indexPath) as? Cell else {
            return UICollectionViewCell() as! Cell
        }
        return cell
    }
}
