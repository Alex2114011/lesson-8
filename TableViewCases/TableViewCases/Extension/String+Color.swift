//
//  String+Color.swift
//  TableViewCases
//
//  Created by Курганов Сергей Владимирович on 21.09.2021.
//

import UIKit

extension String {
    func color() -> UIColor {
        switch self {
        case "red":
            return .red
        case "black":
            return .black
        case "darkGray":
            return .darkGray
        case "lightGray":
            return .lightGray
        case "white":
            return .white
        case "gray":
            return .gray
        case "green":
            return .green
        case "blue":
            return .blue
        case "cyan":
            return .cyan
        case "yellow":
            return .yellow
        case "magenta":
            return .magenta
        case "orange":
            return .orange
        case "purple":
            return .purple
        case "brown":
            return .brown
        default:
            return #colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 1)
        }
    }
}
