//
//  ColoredCarouselBlock.swift
//  TableViewCases
//
//  Created by Курганов Сергей Владимирович on 21.09.2021.
//

import Foundation

struct ColoredCarouselBlock: BlockProtoocol {
    let blockType: BlockType = .coloredСarousel
    let order = 2
    let colors: [String]
    
    init(colors: [String]) {
        self.colors = colors
    }
}
