//
//  InfoBlock.swift
//  TableViewCases
//
//  Created by Курганов Сергей Владимирович on 21.09.2021.
//

import Foundation

struct InfoBlock: BlockProtoocol {
    let blockType: BlockType = .info
    let order = 1
    let description: String
    
    init(description: String) {
        self.description = description
    }
}
