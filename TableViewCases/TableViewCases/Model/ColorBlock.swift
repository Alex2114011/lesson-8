//
//  ColorBlock.swift
//  TableViewCases
//
//  Created by Курганов Сергей Владимирович on 21.09.2021.
//

import Foundation

struct ColorBlock: BlockProtoocol {
    let blockType: BlockType = .color
    let order = 0
    let color: String
    
    init(color: String) {
        self.color = color
    }
}
