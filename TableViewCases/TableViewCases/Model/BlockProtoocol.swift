//
//  BlockProtoocol.swift
//  TableViewCases
//
//  Created by Курганов Сергей Владимирович on 21.09.2021.
//

import Foundation

protocol BlockProtoocol {
    var blockType: BlockType { get }
    var order: Int { get }
}

enum BlockType {
    case info
    case color
    case coloredСarousel
    case displayCase
}
