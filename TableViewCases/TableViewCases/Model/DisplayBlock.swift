//
//  DisplayBlock.swift
//  TableViewCases
//
//  Created by Курганов Сергей Владимирович on 21.09.2021.
//

import Foundation

struct DisplayBlock: BlockProtoocol {
    let blockType: BlockType = .displayCase
    let order = 4
    let title: String
    
    init(title: String) {
        self.title = title
    }
}
