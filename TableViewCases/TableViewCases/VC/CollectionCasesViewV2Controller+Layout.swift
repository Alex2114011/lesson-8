//
//  CollectionCasesViewV2Controller+Layout.swift
//  TableViewCases
//
//  Created by Курганов Сергей Владимирович on 21.09.2021.
//

import UIKit

extension CollectionCasesViewV2Controller {
    func lauout(for type: CasesViewV2Type) -> CGSize {
        switch type {
        case .info: return CGSize(width: view.bounds.width, height: 350)
        case .display: return CGSize(width: view.bounds.width, height: 64)
        case .color: return CGSize(width: view.bounds.width, height: 200)
        case .coloredCarousel: return CGSize(width: view.bounds.width, height: 200)
        }
    }
}
