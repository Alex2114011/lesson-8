//
//  CollectionCasesViewV2Controller.swift
//  TableViewCases
//
//  Created by Курганов Сергей Владимирович on 21.09.2021.
//

import Foundation

import UIKit

enum CasesViewV2Type {
    case info(title: String)
    case display(title: String)
    case color(color: String)
    case coloredCarousel(colors: [String])
    
    // в enum можно модель запихнуть
    //case test(model: InfoBlock)
    
    // в новом swift 5 можно порядко устанваливать для enum
}

class CollectionCasesViewV2Controller: UIViewController {
    private var models: [CasesViewV2Type] = []
    private let dataManager = DataManager()
    private let isLast = true
    
    private lazy var flow: UICollectionViewFlowLayout = { flow in
        flow.scrollDirection = .vertical
        return flow
    }(UICollectionViewFlowLayout())
    
    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: view.bounds,
                                              collectionViewLayout: flow)
        collectionView.registerCell(from: ColorCollectionViewCell.self)
        collectionView.registerCell(from: InfoCollectionViewCell.self)
        collectionView.registerCell(from: ColoredCarouselCollectionViewCell.self)
        collectionView.registerCell(from: DispalyBlockCollectionViewCell.self)
        collectionView.registerCell(from: UICollectionViewCell.self)
        collectionView.backgroundColor = .white
        
        collectionView.delegate = self
        collectionView.dataSource = self
        return collectionView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        fetchData()
    }
}

extension CollectionCasesViewV2Controller {
    private func configure() {
        view.backgroundColor = .white
        view.addSubview(collectionView)
    }
    
    private func fetchData() {
        dataManager
            .fetchData(finish: { [weak self] data in
                guard let self = self else { return }
                let models: [CasesViewV2Type] = self.isLast
                    ? [.color(color: data.color),
                       .coloredCarousel(colors: data.colors),
                       .info(title: data.info)]
                    : [.color(color: data.color),
                       .coloredCarousel(colors: data.colors),
                       .display(title: data.diaplay),
                       .info(title: data.info)]
                
                // datail maping
//                var models: [CasesViewV2Type] = []
//                if !data.color.isEmpty {
//                    models.append(.color(color: data.color))
//                }
//
//                if !data.colors.isEmpty {
//                    models.append(.coloredCarousel(colors: data.colors))
//                }
//
//                if !data.info.isEmpty {
//                    models.append(.display(title: data.info))
//                }
//
//                if !self.isLast {
//                    models.append(.display(title: data.diaplay))
//                }
                
      
                self.models = models
                self.collectionView.reloadData()
            })
    }
}

extension CollectionCasesViewV2Controller: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        lauout(for: models[indexPath.row])
    }
}

extension CollectionCasesViewV2Controller: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        models.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch models[indexPath.row] {
        case .color(let color):
            let cell = collectionView
                .dequeueReusableCell(ColorCollectionViewCell.self,
                                     indexPath: indexPath)
            cell.configure(color: color)
            return cell
            
        case .info(let info):
            let cell = collectionView
                .dequeueReusableCell(InfoCollectionViewCell.self,
                                     indexPath: indexPath)
            cell.configure(description: info)
            return cell
            
        case .coloredCarousel(let colors):
            let cell = collectionView
                .dequeueReusableCell(ColoredCarouselCollectionViewCell.self,
                                     indexPath: indexPath)
            cell.configure(colors: colors, strongSelf: self)
            return cell
            
        case .display(let title):
        let cell = collectionView
            .dequeueReusableCell(DispalyBlockCollectionViewCell.self,
                                 indexPath: indexPath)
            cell.configure(title: title, strongSelf: self)
        return cell
        }
    }
}

extension CollectionCasesViewV2Controller: DispalyBlockCollectionViewCellDelegate {
    func didTapDisplayButton() {
        navigationController?.pushViewController(CollectionCasesViewController(), animated: true)
    }
}

extension CollectionCasesViewV2Controller: ColoredCarouselCollectionViewCellDelegate {
    func didTapColor() {}
}
