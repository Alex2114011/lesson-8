//
//  CollectionCasesViewController.swift
//  TableViewCases
//
//  Created by Курганов Сергей Владимирович on 21.09.2021.
//

import UIKit

class CollectionCasesViewController: UIViewController {
    private var blocks: [BlockProtoocol] = []
    private let dataManager = DataManager()
    
    private lazy var flow: UICollectionViewFlowLayout = { flow in
        flow.scrollDirection = .vertical
        return flow
    }(UICollectionViewFlowLayout())
    
    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: view.bounds,
                                              collectionViewLayout: flow)
        collectionView.registerCell(from: ColorCollectionViewCell.self)
        collectionView.registerCell(from: InfoCollectionViewCell.self)
        collectionView.registerCell(from: ColoredCarouselCollectionViewCell.self)
        collectionView.registerCell(from: DispalyBlockCollectionViewCell.self)
        collectionView.registerCell(from: UICollectionViewCell.self)
        collectionView.backgroundColor = .white
        
        collectionView.delegate = self
        collectionView.dataSource = self
        return collectionView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        fetchData()
    }
}

extension CollectionCasesViewController {
    private func configure() {
        view.backgroundColor = .white
        view.addSubview(collectionView)
    }
    
    private func fetchData() {
        dataManager
            .fetchData(finish: { [weak self] data in
                guard let self = self else { return }
                let color = ColorBlock(color: data.color)
                let colors = ColoredCarouselBlock(colors: data.colors)
                let display = DisplayBlock(title: data.diaplay)
                let info = InfoBlock(description: data.info)
                var blocks: [BlockProtoocol] = [color, colors, display, info]
                blocks.sort(by: { $0.order < $1.order })
                
                self.blocks = blocks
                self.collectionView.reloadData()
            })
    }
}

extension CollectionCasesViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        lauout(for: blocks[indexPath.row].blockType)
    }
}

extension CollectionCasesViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        blocks.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch blocks[indexPath.row] {
        case let model as ColorBlock:
            let cell = collectionView
                .dequeueReusableCell(ColorCollectionViewCell.self,
                                     indexPath: indexPath)
            cell.configure(color: model.color)
            return cell
            
        case let model as InfoBlock:
            let cell = collectionView
                .dequeueReusableCell(InfoCollectionViewCell.self,
                                     indexPath: indexPath)
            cell.configure(description: model.description)
            return cell
            
        case let model as ColoredCarouselBlock:
            let cell = collectionView
                .dequeueReusableCell(ColoredCarouselCollectionViewCell.self,
                                     indexPath: indexPath)
            cell.configure(colors: model.colors, strongSelf: self)
            return cell
            
        case let model as DisplayBlock:
        let cell = collectionView
            .dequeueReusableCell(DispalyBlockCollectionViewCell.self,
                                 indexPath: indexPath)
            cell.configure(title: model.title, strongSelf: self)
        return cell
            
        default: return UICollectionViewCell()
        }
    }
}

extension CollectionCasesViewController: DispalyBlockCollectionViewCellDelegate {
    func didTapDisplayButton() {
        navigationController?.pushViewController(CollectionCasesViewV2Controller(), animated: true)
    }
}

extension CollectionCasesViewController: ColoredCarouselCollectionViewCellDelegate {
    func didTapColor() {}
}
