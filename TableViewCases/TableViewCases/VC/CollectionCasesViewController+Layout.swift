//
//  CollectionCasesViewController+Layout.swift
//  TableViewCases
//
//  Created by Курганов Сергей Владимирович on 21.09.2021.
//

import UIKit

extension CollectionCasesViewController {
    func lauout(for block: BlockType) -> CGSize {
        switch block {
        case .color: return CGSize(width: view.bounds.width, height: 200)
        case .coloredСarousel: return CGSize(width: view.bounds.width, height: 200)
        case .displayCase: return CGSize(width: view.bounds.width, height: 64)
        case .info: return CGSize(width: view.bounds.width, height: 350)
        }
    }
}
