//
//  InfoCollectionViewCell.swift
//  TableViewCases
//
//  Created by Курганов Сергей Владимирович on 21.09.2021.
//

import UIKit

class InfoCollectionViewCell: UICollectionViewCell {
    private var infoLabel: UILabel = { label in
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.8)
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }(UILabel())
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.addSubview(infoLabel)
        
        NSLayoutConstraint.activate([
            infoLabel.topAnchor.constraint(equalTo: topAnchor, constant: 16),
            infoLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16),
            infoLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            infoLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension InfoCollectionViewCell {
    func configure(description: String) {
        infoLabel.text = description
    }
}
