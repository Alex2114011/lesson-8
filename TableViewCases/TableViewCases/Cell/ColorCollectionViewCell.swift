//
//  ColorCollectionViewCell.swift
//  TableViewCases
//
//  Created by Курганов Сергей Владимирович on 21.09.2021.
//

import UIKit

class ColorCollectionViewCell: UICollectionViewCell {
    private var colorView: UIView = { view in
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 12
        return view
    }(UIView())
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.addSubview(colorView)
        
        NSLayoutConstraint.activate([
            colorView.topAnchor.constraint(equalTo: topAnchor, constant: 16),
            colorView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16),
            colorView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            colorView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension ColorCollectionViewCell {
    func configure(color: String) {
        colorView.backgroundColor = color.color()
    }
}

