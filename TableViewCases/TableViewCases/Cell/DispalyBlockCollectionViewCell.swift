//
//  DispalyBlockCollectionViewCell.swift
//  TableViewCases
//
//  Created by Курганов Сергей Владимирович on 21.09.2021.
//

import UIKit

protocol DispalyBlockCollectionViewCellDelegate: AnyObject {
    func didTapDisplayButton()
}

class DispalyBlockCollectionViewCell: UICollectionViewCell {
    private weak var delegate: DispalyBlockCollectionViewCellDelegate?
    
    private var displayButton: UIButton = { button in
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = 16.0
        button.backgroundColor = #colorLiteral(red: 0.03921568627, green: 0.5176470588, blue: 1, alpha: 1)
        return button
    }(UIButton())
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.addSubview(displayButton)
        displayButton.addTarget(self, action: #selector(didTapDisplayButton), for: .touchUpInside)
        
        NSLayoutConstraint.activate([
            displayButton.topAnchor.constraint(equalTo: topAnchor, constant: 16),
            displayButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16),
            displayButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            displayButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension DispalyBlockCollectionViewCell {
    func configure(title: String, strongSelf: DispalyBlockCollectionViewCellDelegate?) {
        displayButton.setTitle(title, for: .normal)
        delegate = strongSelf
    }
    
    @objc
    private func didTapDisplayButton() {
        delegate?.didTapDisplayButton()
    }
}

