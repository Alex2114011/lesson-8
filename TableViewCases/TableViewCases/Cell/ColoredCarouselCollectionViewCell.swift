//
//  ColoredCarouselCollectionViewCell.swift
//  TableViewCases
//
//  Created by Курганов Сергей Владимирович on 21.09.2021.
//

import UIKit

protocol ColoredCarouselCollectionViewCellDelegate: AnyObject {
    func didTapColor()
}

class ColoredCarouselCollectionViewCell: UICollectionViewCell {
    private weak var delegate: ColoredCarouselCollectionViewCellDelegate?
    
    private var colors: [String] = []
    
    private lazy var flow: UICollectionViewFlowLayout = { flow in
        flow.scrollDirection = .horizontal
        return flow
    }(UICollectionViewFlowLayout())
    
    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero,
                                              collectionViewLayout: flow)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.registerCell(from: ColorCollectionViewCell.self)
        collectionView.registerCell(from: UICollectionViewCell.self)
        collectionView.backgroundColor = .white
        collectionView.showsHorizontalScrollIndicator = false
        
        collectionView.delegate = self
        collectionView.dataSource = self
        return collectionView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.addSubview(collectionView)
        
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: topAnchor, constant: 16),
            collectionView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16),
            collectionView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            collectionView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension ColoredCarouselCollectionViewCell {
    func configure(colors: [String], strongSelf: ColoredCarouselCollectionViewCellDelegate?) {
        self.colors = colors
        delegate = strongSelf
        collectionView.reloadData()
    }
    
    @objc
    private func didTapColor() {
        delegate?.didTapColor()
    }
}

extension ColoredCarouselCollectionViewCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        CGSize(width: 82, height: 164)
    }
}

extension ColoredCarouselCollectionViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        colors.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(ColorCollectionViewCell.self, indexPath: indexPath)
        cell.configure(color: colors[indexPath.row])
        return cell
    }
}
