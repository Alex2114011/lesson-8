//
//  DataManager.swift
//  TableViewCases
//
//  Created by Курганов Сергей Владимирович on 21.09.2021.
//

import Foundation

class DataManager {
    var dispatchGroup: DispatchGroup?
    private var dataCases = DataCases()
    private let colors: [String] = ["red", "black", "darkGray", "lightGray", "white", "gray", "green",
                                    "blue", "cyan", "yellow", "magenta", "orange", "purple", "brown"]
    
    
    func fetchData(finish: @escaping(DataManager.DataCases) -> Void) {
        dispatchGroup = DispatchGroup()
        
        dispatchGroup?.enter()
        fetchInfo(finish: { [weak self] info in
            self?.dataCases.info = info
            self?.dispatchGroup?.leave()
        })
        
        dispatchGroup?.enter()
        fetchColor(finish: { [weak self] color in
            self?.dataCases.color = color
            self?.dispatchGroup?.leave()
        })
        
        dispatchGroup?.enter()
        fetchColors(finish: { [weak self] colors in
            self?.dataCases.colors = colors
            self?.dispatchGroup?.leave()
        })
        
        dispatchGroup?.enter()
        fetchDisplay(finish: { [weak self] text in
            self?.dataCases.diaplay = text
            self?.dispatchGroup?.leave()
        })
        
        dispatchGroup?.notify(queue: DispatchQueue.main, execute: { [weak self] in
            guard let strongSelf = self else { return }
            finish(strongSelf.dataCases)
        })
    }
    
    struct DataCases {
        var color: String = ""
        var colors: [String] = []
        var diaplay: String = ""
        var info: String = ""
    }
}
extension DataManager {
    private func fetchInfo(finish: @escaping(String) -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
            let info = """
                       Rick Deckard, a bounty hunter for the San Francisco Police Department, is assigned to "retire" (kill) six androids of the new and highly intelligent Nexus-6 model which have recently escaped from Mars and traveled to Earth. These androids are made of organic matter so similar to a human's that only a posthumous "bone marrow analysis" can independently prove the difference, making them almost impossible to distinguish from real people.
                       """
            finish(info)
        })
    }
    
    private func fetchColor(finish: @escaping(String) -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
            finish(self.colors.randomElement() ?? "red")
        })
    }
    
    private func fetchColors(finish: @escaping([String]) -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.2, execute: {
            finish(self.colors)
        })
    }
    
    private func fetchDisplay(finish: @escaping(String) -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: {
            finish("Push me!")
        })
    }
}
